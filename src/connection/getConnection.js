const mysql = require('mysql');
const config = require('./connection.js');

const pool = mysql.createPool(config);

var getConnection = (callback)=>{
    pool.getConnection((err, connection)=>{
        callback(err, connection);
            connection.release();
    });
}

module.exports = getConnection;