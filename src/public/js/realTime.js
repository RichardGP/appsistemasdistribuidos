$(document).ready(()=>{
    const socket = io();
    
    socket.on('sensorNode', function(dataReceiver){
 
        
        let fecha = new Date(dataReceiver.timeSensor);

        let hora = fecha.getHours();
        let minutos = fecha.getMinutes();
        let segundos = fecha.getSeconds();

        let fechaActual = `${hora}:${minutos}:${segundos}`;

        myChart.data.labels.push(fechaActual);
        myChartVT.data.labels.push(fechaActual);
        if(myChart.data.labels.length > 10){
            myChart.data.labels.shift();
            myChartVT.data.labels.shift();
        }
        myChart.data.datasets.forEach((element, index) => {
            element.data.push(dataReceiver.valueSensor);
            if(element.data.length > 10){
                element.data.shift();       
            }       
        });
        myChartVT.data.datasets.forEach((element, index)=>{
            element.data.push(dataReceiver.voltajeSensor);
            if(element.data.length > 10){
                element.data.shift();       
            }       
        });
        
        myChart.update();
        myChartVT.update();
    });

    var ctx = document.getElementById('myCanvas').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            
            labels:[], //Pasa el tiempo en un array
            
            datasets: [{
                label: 'Gas level',
                data: [], //para el nivel de gas en un array
                backgroundColor: 'rgba(0, 0, 0, 0.1)',
                borderColor: 'rgba(240, 15, 39)',
                pointRadius: 5,
                pointBackgroundColor: 'rgba(14, 1, 10)',
                pointHoverRadius: 8,
                pointHoverBackgroundColor: 'rgba(200, 9, 35)',
            }]
        },
        
        options: {
            title: {
            display: true,
            text: 'Grafica Nivel de Gas',
            fontSize: 25,
            padding: 10
        },
        layout: {
        padding: {
            left: 130,
            right: 130,
            top: 50,
            bottom: 0
        }
        },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        min: 0
                    }
                }]
            }
        }
    });

    //GraficaVoltaje-Tiempo
    var context = document.getElementById('myCanvasVT').getContext('2d');
    var myChartVT = new Chart(context, {
        type: 'line',
        data: {
            
            labels:[], 
            
            datasets: [{
                label: 'V-T',
                data: [], 
                backgroundColor: 'rgba(0, 0, 0, 0.1)',
                borderColor: 'rgba(240, 15, 39)',
                pointRadius: 5,
                pointBackgroundColor: 'rgba(14, 1, 10)',
                pointHoverRadius: 8,
                pointHoverBackgroundColor: 'rgba(200, 9, 35)',
            }]
        },
        
        options: {
            title: {
            display: true,
            text: 'Grafica Voltaje-tiempo',
            fontSize: 25,
            padding: 10
        },
        layout: {
        padding: {
            left: 130,
            right: 130,
            top: 50,
            bottom: 0
        }
        },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        min: 0
                    }
                }]
            }
        }
    });

    //GraficaHistorica
    var ct = document.getElementById('myCanvasHistorico').getContext('2d');
    var myChart2 = new Chart(ct, {
        type: 'line',
        data: {
            
            labels:[], //Pasa el tiempo en un array
            
            datasets: [{
                label: 'Gas level historico',
                data: [], //para el nivel de gas en un array
                backgroundColor: 'rgba(0, 0, 0, 0.1)',
                borderColor: 'rgba(240, 20, 39)',
                pointRadius: 5,
                pointBackgroundColor: 'rgba(14, 1, 10)',
                pointHoverRadius: 8,
                pointHoverBackgroundColor: 'rgba(200, 9, 35)',
            }]
        },
        
        options: {
            title: {
            display: true,
            text: 'Grafica Historica',
            fontSize: 25,
            padding: 10
            },
            layout: {
                padding: {
                    left: 25,
                    right: 25,
                    top: 50,
                    bottom: 0
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        min: 0
                    }
                }]
            }
        }
    }); 

    
    $('form input[type="button"]').click(()=>{
        myChart2.data.labels = [];

        myChart2.data.datasets.forEach((element, index) => {
            element.data = [];      
        });

        let fecha = $('#fecha').val();

        socket.emit('historicoSend', fecha);
 
    });

    socket.on('Response', (dataReceiver)=>{
        console.log(dataReceiver);
        for(let data in dataReceiver){
            let fecha = new Date(dataReceiver[data].timeSensor);
            let hora = fecha.getHours();
            let minutos = fecha.getMinutes();
            let segundos = fecha.getSeconds();
    
            let fechaActual = `${hora}:${minutos}:${segundos}`;
            myChart2.data.labels.push(fechaActual);

            myChart2.data.datasets.forEach((element, index) => {
                element.data.push(dataReceiver[data].valueSensor);      
            });
            
        }
        myChart2.update();
    });
   
    $('form input[name="date"]').datepicker({
        dateFormat:'yy-mm-dd'
    });
    
});